#!/usr/bin/env python3
 

import numpy as np
import rospy
from pynput.keyboard import Key, Listener

from std_srvs.srv import SetBool, SetBoolRequest

from ros_wrap.com_manager import *

from msg_srv_action_gestion.srv import GetFloat, GetFloatRequest

import time

global isPressed 
global isReleased
global stopCall


def on_press(key):
    ## pressed : when keep pressed, will keep calling
    ## this method
    global isPressed
    

    canProvokePause = ['a', 'z', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'q', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'w', 'x', 'c', 'v', 'b', 'n',
                        '1', '2', '3', '4', '5', '6', '7', '8', '9','0'
                        ]

    keyPause = [Key.alt, Key.tab, Key.space,Key.backspace, Key.delete, 
                Key.up, Key.down, Key.left, Key.right, Key.enter]    



    if rospy.is_shutdown():
        return(False)
    
    # print('{0} pressed'.format(
    #     key))
    
    str_key = str('{0}'.format(key))
    # print(len(str_key))


    if key in keyPause:
        isPressed = True
    elif len(str_key) == 3:
        str_key = str_key[1]
        if str_key in canProvokePause:
            isPressed = True
    elif len(str_key) == 7:
            str_key = str_key[2]
            if str_key in canProvokePause:
                isPressed = True
            

def on_release(key):
    ## release : one-time only.
    global isReleased
    isReleased = True

    if rospy.is_shutdown():
        return(False)
    # print('{0} release'.format(
    #     key))
    
    if key == Key.shift_r:
        ## call for stopTimer
        global stopCall
        stopCall = True

    # if key == Key.esc:
    #     # Stop listener
    #     return False


## first isPressed : 
##      - call for pause
##      - wait for a isReleased
##      - if isReleased without isPressed after 1.0 seconds ==> restart timer
## poss : put isPressed and isReleased as counter
## on how many there as pressed / released.
## if they are same number ==> start clockdown of 1.0 sec
## ===> pas une bonne idée : qd on laisse pressed enfoncé, continue d'incrémenter
##
## autre poss : isPressed / isReleased en tant que liste de chaîne de caractères, où on met toutes les lettres.
## Lettres ne peuvent pas se répêter. 
## Lorsque lettre se trouve à la fois dans isPressed / isReleased ==> supprimé des 2 listes
## Lorsque les deux listes sont vides en même temps ==> start clockdown of 1.0 sec.
class keyboardRecorder:


    def __init__(self):
        global isPressed
        global isReleased
        global stopCall
        isPressed = False
        isReleased = False
        stopCall = False

        with Listener(
            on_press=on_press,
            on_release=on_release) as listener:

            rospy.init_node("custom_marker")
            nodeName = rospy.get_name()
            nodeName+="/"

            state = 0
            timeAtPause = -1

            canStartCounting = False
            self.cM = comManager()
            timeAtPause = rospy.Time.now().to_sec()

            rate = rospy.Rate(100)
            
            delayForPause = rospy.get_param(nodeName+"pause_delay",1.0)

            getTimer = rospy.get_param("/activate_timer", -1.0)

            if getTimer == -1.0:
                ## By default, desactivates timer
                getTimer = False               

            if not getTimer:
                rospy.logwarn("Timer not set, desactivates the following of the keyboard.")
                #listener.join()
                exit()

            else:

                ## Clients
                self.pctClient = rospy.ServiceProxy("/pause_continue_timer", SetBool)

                self.stopTimer = rospy.ServiceProxy("/stop_timer", GetFloat)

                rospy.loginfo("Initialization done for keyboard recorder!")

                isPressed = False
                isReleased = False

                while not rospy.is_shutdown():
                    if stopCall:
                        ## call for stopping timer

                        req = GetFloatRequest()
                        suc = False
                        while not rospy.is_shutdown() and not suc:
                            suc,resp,ret = self.cM.call_service("stop_timer",self.stopTimer,req)

                        if suc:
                            stopCall = False

                    if state == 0:
                        if isPressed:
                            isPressed = False
                            ## first call 
                            ## pause timer
                            state+=1
                    
                    elif state == 1:
                        ## call for pause
                        suc,resp1,ret = self.cM.call_service("pause_continue_timer", self.pctClient, True)

                        if suc:
                            state+=1
                    
                    elif state ==2:
                        ## 
                        if isPressed:
                            isPressed = False
                            canStartCounting = False
                        
                        if isReleased:
                            timeAtPause = rospy.Time.now().to_sec()
                            canStartCounting = True
                            isReleased = False
                        
                        if canStartCounting:

                            if ( rospy.Time.now().to_sec() - timeAtPause) > delayForPause:
                                canStartCounting = False
                                state+=1

                    
                    elif state == 3:
                        ## restart timer

                        suc,resp1,ret = self.cM.call_service("pause_continue_timer", self.pctClient, False)
                        if suc:
                            state = 0

                    rate.sleep()

                listener.join()




if __name__ == "__main__":
    Kr = keyboardRecorder()

    # Collect events until released
    # with Listener(
    #         on_press=on_press,
    #         on_release=on_release) as listener:
    #     listener.join()